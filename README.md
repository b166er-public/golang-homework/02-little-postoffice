# The little post office

In this homework we will write a simulator for a small **post office** which might be found in bigger village or even a city.

As soon as the post office opens it's door, the **customer** can enter the building and go to the **office worker** to ask for help to complete his/her **request**.

We will start with a **post office** where only 4 **office worker** will work in parallel. But be prepared that later we also can build a **post office** where more than 4 **office worker** will work (of maybe even less than 4 - who know how good things will run for our little postoffice in future).

Imagine following plan of our **post office** ...

![Plan of postoffice](plan.png "Plan of postoffice")

Following behaviour rules are important to know when you want to be good **customer** :

- As a **customer** you enter the postoffice through the entrance
- As a **customer** you can see if a **box** is free or the worker in the box is currently busy
- In every **box** during the complete opening hours of the **post office** exactly one **office worker** will sit and help you with your **request** as soon as you enter his/her box
- If a customer enter the **post office** and all **box**'es are busy, than the **customer** will wait next to the entrance (***blue area***) in a queue
- As soon the **office worker** completed the **request** of a **customer**, the **customer** immediately leave the box and the the **post office** through the exit
- Every customer enters the **post office** with exactly one **request**

The **post office** can help the customers only with this **request**'s :

- **Sending a letter** will take 3 minutes for an **office worker**
- **Sending a parcel** will take 5 minutes for an **office worker**
- **Sending a big parcel** will take 10 minutes for an **office worker**
- **Buying some stamps** will take 2 minutes for an **office worker**

In the end of the working day our program should give us following information:

- How many minutes the **office worker**'s had effectly work?
- How many minutes the **office worker**'s did nothing and only waited for the next **customer**
- How many minutes the **post office** was opened
- How many **customers** visited the **post office** during this working day?
- How long was the queue of **customers** in most busiest moment during this working day?