package main

import "fmt"

type CustomerRequest uint

const (
	SENDING_A_LETTER     CustomerRequest = 0
	SENDING_A_PARCEL     CustomerRequest = 1
	SENDING_A_BIG_PARCEL CustomerRequest = 2
	BUYING_SOME_STAMPS   CustomerRequest = 3
)

// This method tell us, how long this request will take time for a office worker to finish (in minutes)
func (this *CustomerRequest) NeedsTime() uint {
	switch *this {
	case SENDING_A_LETTER:
		return 3 // 3 min for sending a letter
	case SENDING_A_PARCEL:
		return 5 // 5 min for sending a parcel
	case SENDING_A_BIG_PARCEL:
		return 10 // 10 min for sending a big parcel
	case BUYING_SOME_STAMPS:
		return 2 // 2 min for buying stamps
	default:
		// Services which the post office do not offer takes no time as the customer is asked to leave the building
		return 0
	}
}

// Here we model the structura aspects of a customer of a post office
type Customer struct {
	request *CustomerRequest

	timeWithWorker uint
}

// This function creates a new customer with a request.
// The result of this function is a customer which is ready to enter the post office building
func NewCustomer(request CustomerRequest) *Customer {
	return &Customer{request: &request, timeWithWorker: uint(request.NeedsTime())}
}

// If the post office is visited by more customers, than the office workers can handle they have to wait in a queue
// This type tries to model such a queue
// Hint : A queue is always working in FIFO (First In = First Out) mode. That mean, that the first customer which will
//
//	enter the queue, will also leave the queue before anyone else in the queue
type CustomerQueue struct {
	customers []*Customer
}

// This function creates an empty queue
func NewCustomerQueue() *CustomerQueue {
	customer_queue := make([]*Customer, 0)
	return &CustomerQueue{customer_queue}
}

// This method add a new customer into the customer queue
func (this *CustomerQueue) AddNewCustomer(customer *Customer) {
	this.customers = append(this.customers, customer)
}

// This method removes the customer from the queue which spend the most time there
func (this *CustomerQueue) CallNextCustomer() *Customer {
	next_customer := this.customers[0]
	this.customers = this.customers[1:]
	return next_customer
}

// This method tells us, how many customers are currently in the queue
func (this *CustomerQueue) CountCustomers() uint {
	return uint(len(this.customers))
}

// Here we model structural aspects of an office worker which works in an post office
type OfficeWorker struct {
	isWorking bool
	
	currentCustomer uint

	servedCustomers int

	humanMinutesWorking int

	restingTime int

}

// Here we model structrual aspects of a post office
type PostOffice struct {
	allMinutes uint

	queue *CustomerQueue

	maxQueue uint

	allWorkersWorkingTime uint

	allWorkersRestingTime uint
	
	workers []*OfficeWorker
}

// This function create a new post office with 4 office workers and an empty queue
func NewPostOffice() *PostOffice {
	result := &PostOffice{
		allMinutes: 0,
		maxQueue: 0,
		queue: NewCustomerQueue(),
		allWorkersWorkingTime: 0,
		allWorkersRestingTime: 0,

	}

	for i := 0; i < 4; i++ {
		worker := &OfficeWorker{
			isWorking: false,
			currentCustomer: 0,
			servedCustomers: 0,
			humanMinutesWorking: 0,
			restingTime: 0,
		}
		result.workers = append(result.workers, worker)
	}

	return result // Remove this line a replace with correct solution
}

// This method tells us, how long the post office is already opened today
func (this *PostOffice) OpenedMinutes() uint {
	return uint(this.allMinutes)
}

// This method tells us, how many customers are inside the post office
// Hint : Do not forget to count the customers which are currently talking with the office workers
//
//	and the customers waiting the queue
func (this *PostOffice) CustomerInside() uint {
	customersInside := uint(len(this.queue.customers))
	for _, worker := range this.workers {
		if worker.isWorking {
			customersInside++
		}
	}
	return customersInside
}

// This method allows a new customer enter the post office
func (this *PostOffice) CustomerEntersPostOffice(customer *Customer) {
	this.queue.customers = append(this.queue.customers, customer)
	return
}

// This method tells us how much time all the workers together in the post office
// worked today.
func (this *PostOffice) HowLongAllOfficeWorkersWorked() uint {
	total := 0
    for _, worker := range this.workers {
        	total += worker.humanMinutesWorking
    }
    return uint(total) // Remove this line a replace with correct solution
}

// This method tells us how much time all the workers together in the post office
// were waiting for a customer.
func (this *PostOffice) HowLongAllOfficeWorkersWaited() uint {
	totalWaitTime := 0
    for _, worker := range this.workers {
        totalWaitTime += worker.restingTime
    }
    return uint(totalWaitTime)
}

// This method tells us, how long was the maximum size of the customer queue
// Hint : If suddenly 50 customer enters the empty post office, 4 of them directly go to the office workers, while 46 will stay in the customer queue
func (this *PostOffice) MaxCustomerQueueSize() uint {
	return this.maxQueue
}

// This method tells us, how many customers had visit the post office today
// Hint : Simply sum up all the customers which were helped by all the office workers in this post office
func (this *PostOffice) CustomersVisitedPostOfficeDuringDay() uint {
	result := 0
	for _, worker := range this.workers {
		result += worker.servedCustomers
	}
	return uint(result)
}

// This is a very important method. This method simulates than 1 min has passed
func (this *PostOffice) OneMinuteLater() {
	// Check if there a workers who have no customer currently -> If so, send customers (if any) in customer queue to this worker
	for _, worker := range this.workers {
		if !worker.isWorking && len(this.queue.customers) > 0 {
			// Assign a customer to this worker
			customer := this.queue.CallNextCustomer()
			worker.isWorking = true
			worker.currentCustomer = customer.timeWithWorker
			worker.servedCustomers++
		}
	}

	// Update working minutes of every office worker
	// If the office worker has no customer he/she is just waiting for a new one
	// If the office worker has a customer now, he/she is working
	for _, worker := range this.workers {
		if worker.isWorking {
			worker.humanMinutesWorking++
			worker.currentCustomer--
		} else{
			worker.restingTime++
		}
	}

	// Check if there are workers who finished working with a customer
	// Hint : The worker finished working on a request of a customer as soon as the time elapsed for this request
	//        For example : After 5 minutes every office worker finished sending a parcel
	for _, worker := range this.workers {
		if worker.isWorking && worker.currentCustomer == 0 {
			worker.isWorking = false
		}
	}

	// Update maximum queue size of the post office
	// If now the customer queue is bigger then every, save this length for later
	lastSize := this.maxQueue
	newSize := this.queue.CountCustomers()

	if newSize > lastSize {
		this.maxQueue = newSize
	}



	// Update our opened minutes
	this.allMinutes++

	// Every time this method is called, 1 min passes
}

// ------------------------------------------------------------------------------------------------------------------------//
/**
* From here the tests begin
* ATTENTION : From this line on no changes are allowed.
 */

func working_day_1() bool {
	// Post Office opens it' doors.
	post_office := NewPostOffice()

	// No customer should be inside now
	if post_office.CustomerInside() != 0 {
		return false
	}

	// The office just opened
	if post_office.OpenedMinutes() != 0 {
		return false
	}

	// In the first minute we have no customer ...
	post_office.OneMinuteLater()

	// The office is now one minute opened ...
	if post_office.OpenedMinutes() != 1 {
		return false
	}

	// All workers had been waited for their first customer?
	// (Every worker spend one minute waiting) * (There are 4 office workers) = 4 minutes
	if post_office.HowLongAllOfficeWorkersWaited() != 4 {
		return false
	}

	// Hurray ... our first customers comes in
	post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_LETTER))

	// Now there should be one customer in the post office
	if post_office.CustomerInside() != 1 {
		return false
	}

	// Lets pass the second minute ...
	post_office.OneMinuteLater()

	// Now the office worker who got the first customer is working, the rest if waiting
	if post_office.HowLongAllOfficeWorkersWorked() != 1 {
		return false
	}
	if post_office.HowLongAllOfficeWorkersWaited() != 7 {
		// Hint: 7 min because: 4 min in the first minute of openening and now 3 min as 3 worker still wait
		return false
	}

	// Let's wait until the customer leaves the post office
	for post_office.CustomerInside() > 0 {
		post_office.OneMinuteLater()
	}

	// For the first day it should be enough the boss said and decided to close the post office for this day
	if post_office.OpenedMinutes() != 4 {
		return false
	}
	if post_office.HowLongAllOfficeWorkersWorked() != 3 {
		return false
	}
	if post_office.HowLongAllOfficeWorkersWaited() != 13 {
		return false
	}

	return true
}

func working_day_2() bool {
	// Post Office opens it' doors.
	post_office := NewPostOffice()

	// Right in the beginning 4 customers comes in, and all of them wants to send a parcel
	post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_PARCEL))
	post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_PARCEL))
	post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_PARCEL))
	post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_PARCEL))

	// We wait until all customers left the office
	for post_office.CustomerInside() > 0 {
		post_office.OneMinuteLater()
	}

	// We expect that all customer left after 5 minutes
	if post_office.OpenedMinutes() != 5 {
		return false
	}

	// Now suddenly out of nowhere 20 customers comes inside and want to buy some stamps ...
	for i := 0; i < 20; i++ {
		post_office.CustomerEntersPostOffice(NewCustomer(BUYING_SOME_STAMPS))
	}

	// Lets wait until all this customers left our post office
	for post_office.CustomerInside() > 0 {
		post_office.OneMinuteLater()
	}

	// We expect that to work all customers through we will need 10 min more (2 min per request, 4 workers in parallel, 5 groups of customers)
	// Thus after (10 min + 5 min = 15 min) we should be ready
	if post_office.OpenedMinutes() != 15 {
		return false
	}

	// This was a stressfull 15 min where no one had even a minute for a rest, right?
	if post_office.HowLongAllOfficeWorkersWaited() != 0 {
		return false
	}

	// We had even a moment, were the queue contained 16 customers ...
	if post_office.MaxCustomerQueueSize() != 16 {
		return false
	}

	// And thus the boss decided to close for today
	return true
}

func working_day_3() bool {
	// Post Office opens it' doors.
	post_office := NewPostOffice()

	// Right after the doors of the post office opened 100 customers ran in

	// ... 50 of them wanted to send normal parcels
	for i := 0; i < 50; i++ {
		post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_PARCEL))
	}

	// ... 50 of them wanted to send big parcels
	for i := 0; i < 50; i++ {
		post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_BIG_PARCEL))
	}

	// Then 30 minutes lates ...
	for i := 0; i < 30; i++ {
		post_office.OneMinuteLater()
	}

	// Another 100 customers came in ...

	// ... 50 of them wanted to send a letter
	for i := 0; i < 50; i++ {
		post_office.CustomerEntersPostOffice(NewCustomer(SENDING_A_LETTER))
	}

	// ... 50 of them wanted to buy some stamps
	for i := 0; i < 50; i++ {
		post_office.CustomerEntersPostOffice(NewCustomer(BUYING_SOME_STAMPS))
	}

	// The boss looked at this epic queue and waited until all the customers are finished
	for post_office.CustomerInside() > 0 {
		post_office.OneMinuteLater()
	}

	// As the boss saw how much stress the worker had he closed for today as he saw:

	// ... no one had even 1 min time for rest
	if post_office.HowLongAllOfficeWorkersWaited() != 0 {
		return false
	}

	// ... the customer queue had in the worst minute if 172 customers
	if post_office.MaxCustomerQueueSize() != 172 {
		return false
	}

	// ... the post office could finish working with 200 customers
	if post_office.CustomersVisitedPostOfficeDuringDay() != 200 {
		return false
	}

	// ... and needed for all their work just X minutes
	if post_office.OpenedMinutes() != 250 {
		return false
	}

	return true

}

func main() {
	if working_day_1() {
		fmt.Println("Working day 1 ... OK!")
	} else {
		fmt.Println("Working day 1 ... FAILED!")
	}

	if working_day_2() {
		fmt.Println("Working day 2 ... OK!")
	} else {
		fmt.Println("Working day 2 ... FAILED!")
	}

	if working_day_3() {
		fmt.Println("Working day 3 ... OK!")
	} else {
		fmt.Println("Working day 3 ... FAILED!")
	}

}
